simple-plotter-qt
=================

Qt based GUI front-end for the plot code generator `simple-plotter`_.

*simple-plotter-qt* is released under the GPLv3+ license.
For details see the LICENSE file.

Getting started
---------------

It can be installed with pip:

::

    pip install simple-plotter-qt

It depends on:

* simple-plotter
* PyQt5
* matplotlib
* setuptools_scm

Launching the application
-------------------------

setup-tools will install a script called *simple-plotter-qt*.
It can be launched via a terminal:

::

    simple-plotter-qt

Further information
-------------------

For further information please visit the *simple-plotter* documentation:

https://simple-plotter.readthedocs.io/en/latest

.. _simple-plotter: https://gitlab.com/thecker/simple-plotter